import React, {createRef,  Component } from 'react';
import logo from './logo.svg';
import './App.css';
import L from 'leaflet';
import ReactDOM from 'react-dom';
import floorplan from './floorplanSISLvl5.svg';
import prima from './SVGTest-02.svg';
class App extends React.Component {

  componentDidMount() {
      // var map = this.map = L.map(ReactDOM.findDOMNode(this), {
      //     minZoom: 2,
      //     maxZoom: 20,
      //     layers: [
      //         L.tileLayer(
      //             'https://0a25da15.ngrok.io/services/indonesia/map',
      //             {attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'})
      //     ],
      //     attributionControl: false,
      // });

      // map.on('click', this.onMapClick);
      // map.fitWorld();
      //create the map
      
      var map = L.map('map', {
        center: [40.75, -74.2],
        zoom: 14,
        zoomControl: true,

      });
      function adjustSVGElements()
      {
          console.log(map);
      }
      L.svg().addTo(map)
      function onMapClick(){
        console.log('ccc');
      }
      map.on("viewreset", adjustSVGElements);
      map.on('click', onMapClick);
      // // create the image
      // var imageUrl = floorplan,
      //   imageBounds = [[40.712216, -74.22655], [40.773941, -74.12544]];

      // L.imageOverlay(imageUrl, imageBounds).addTo(map);
//---creates an svg element---
    var MySVG 
    MySVG= document.querySelector("svg") //---access svg element---
    // MySVG.setAttribute("cursor", "default") //--arrow---
    // //---zooming the map's SVG elements---
    // MyMap.on("viewreset", adjustSVGElements);
    // //---contains svg paths---
    // SvgElemG=document.createElementNS(NS,"g")
    // SvgElemG.setAttribute("id","svgElemG")
    // MySVG.appendChild(SvgElemG)

    // Wrapper=document.createElementNS(NS,"svg")
    // Wrapper.setAttribute("id","wrapper")
    // MySVG.appendChild(Wrapper)

    // showMapKeyLine()
    // callSVGFile()
      var url = prima;
      var req = new XMLHttpRequest();
      req.onload = function(resp) {
          var xml = this.responseXML;
          var importedNode = document.importNode(xml.documentElement, true);
          console.log('sss',map );
          // var g = document.createElementNS("http://www.w3.org/2000/svg", "g");
          // g.appendChild(importedNode);
          // g.setAttribute('class', 'svglayer');

          // var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
          // svg.appendChild(g);
          // svg.addEventListener('click', function(e) {
          //     console.log(e.target.id)
          // })
          // for (var i = 0; i < importedNode.childNodes.length; i++) {
          //   var child = importedNode.children[i];
          //   MySVG.appendChild(child);
            
          // }
          MySVG.appendChild(importedNode);
      };
      req.open("GET", url, true);
      req.send();
  }

  componentWillUnmount() {
      this.map.off('click', this.onMapClick);
      this.map = null;
  }

  onMapClick = () => {
      // Do some wonderful map things...
  }

  render() {
      return (
          <div id='map'></div>
      );
  }

}

export default App;
